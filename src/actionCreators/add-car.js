import { ADD_CAR, ADD_CAR_SUCCESS, ADD_CAR_FAILURE } from '../actionTypes/add-car';

export function addCar(payload) {
  return {
    type: ADD_CAR,
    payload,
  };
}
export function addCarSuccess(response) {
  return {
    type: ADD_CAR_SUCCESS,
    response,
  };
}
export function addCarFailure(error) {
  return {
    type: ADD_CAR_FAILURE,
    error,
  };
}

