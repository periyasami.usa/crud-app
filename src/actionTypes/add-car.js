export const ADD_CAR = 'ADD_CAR';

export const ADD_CAR_SUCCESS = 'ADD_CAR_SUCCESS';

export const ADD_CAR_FAILURE = 'ADD_CAR_FAILURE';
