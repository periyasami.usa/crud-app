import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as carAction from './../../actionCreators/add-car';

import './add-car.css';

class AddCar extends Component {
    state = {
      carName: '',
      carModel: '',
      carModelYear: '',
    }

    onChange = (e) => {
      this.setState({
        [e.target.name]: e.target.value,
      });
    }

    // componentWillReceiveProps(nextProps) {
    //   if(this.)
    // }
    
    onSubmit = (e) => {
      e.preventDefault();
      this.props.actions.addCar(this.state);
    }

    render() {
      return (
        <div>
          <h3 className="add-form-title">Add Form</h3>
          <hr />
          <form>
            <lable>Car Name</lable>
            <input
              name="carName" 
              placeholder="Car Name" 
              value={this.state.carName}
              onChange={e => this.onChange(e)}
            />
            <br />
            <lable>Car Model</lable>
            <input 
              name="carModel"
              placeholder="Car Model" 
              value={this.state.carModel}
              onChange={e => this.onChange(e)}
            />
            <br />
            <lable>Car Model Year</lable>
            <input
              name="carModelYear" 
              placeholder="Car Model Year" 
              value={this.state.carModelYear}
              onChange={e => this.onChange(e)}
            />
            <br />
            <button type="submit" onClick={e => this.onSubmit(e)} className="btn btn-primary btn-danger submit-btn">Submit</button>
          </form>
        </div>
      );
    }
}


function mapStateToProps(state) {
  return {
    
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(carAction, dispatch),
  };
}

export default (connect(mapStateToProps, mapDispatchToProps)(AddCar));
