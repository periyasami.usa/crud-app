import { combineReducers } from 'redux';
import carsListReducer from './car-list.reducer';
import addCarReducer from './add-car.reducer';

const rootReducer = combineReducers({
  carsListState: carsListReducer,
  addCar: addCarReducer,
});

export default rootReducer;
