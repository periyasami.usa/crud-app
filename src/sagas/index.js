import { put, takeEvery } from 'redux-saga/effects';
import { GET_CARS } from '../actionTypes/cars-list';
import { ADD_CAR } from '../actionTypes/add-car';
import { getCarsListSuccess, getCarsListFailure } from './../actionCreators/cars-list';
import { addCarSuccess, addCarFailure } from './../actionCreators/add-car';
import { push } from 'react-router-redux';


const url = 'http://localhost:3000/cars';
const urlPostCar = 'http://localhost:3000/cars';

function* getCars() {     
  try {
    const carsListData = yield fetch(url).then(r => r.json());
    yield put(getCarsListSuccess(carsListData));
  } catch (error) {
    yield put(getCarsListFailure(error));
  }
}


/* send POST request to add New car data */

function* addNewCar(param) {
  const newCar = param.payload;
  try {
    const response = yield fetch(urlPostCar, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: newCar.carName,
        model: newCar.carModel,
        year: newCar.carModelYear,
      }),
    });
    yield put(addCarSuccess(response));
    yield put(push('/'));
  } catch (error) {    
    yield put(addCarFailure(error));
  }
}

export default function* rootSaga() {
  // const locationChange = yield take(LOCATION_CHANGE);  
  yield [
    takeEvery(GET_CARS, getCars),
    takeEvery(ADD_CAR, addNewCar),
  ];
}
